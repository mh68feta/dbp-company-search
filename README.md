# Wkipedia-based Company Search

Search engine based on structure DBpedia and Wikipedia full text data.

* Project [Report](https://docs.google.com/document/d/1hELPAioK96h9IqLggqNHeuOQETaom7maLHlp3AAliSU/edit?usp=sharing)

## Setup

```
# install prerequisites
sudo apt-get install git git-lfs python3 python3-pip

# clone git
git clone https://git.informatik.uni-leipzig.de/mh68feta/dbp-company-search.git
cd dbp-company-search-master
git-lfs pull

# docker (better use https://docs.docker.com/install/)
apt-get install docker docker-compose
```

run 
```
docker-compose up -d
```

load data (workaround till data can be initialized through a faster way)
```
cd InformationRetrieval
pip3 install -r requirements.txt
python3 postData.py company_data_final.json.bz2
```


Then navigate to  http://localhost:80

To reach solr -> [8983](http://localhost:8983) 

To reach the backend -> logs: [5000/log](http://localhost:5000/log) & evaluation: [5000/eval](http://localhost:5000/eval)


### notes
- "react-geocode": "^0.1.2", https://www.npmjs.com/package/react-geocode?!
