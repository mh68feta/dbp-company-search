#!/usr/bin/python3
import os
import json
import redis
import pysolr
import datetime
import re
import numpy as np

from flask_cors import CORS
from flask_restful import Resource, Api
from flask import Flask, request, jsonify, redirect

redisIp = '127.0.0.1'
solrIp = '172.18.18.4'


###
# Helper Methods
###

def toHTMLTable(lines, delim):
    tableString = "<table style='width:100%'>"
    for i, line in enumerate(lines):
        split = re.split(delim, line)
        tableString += "<tr>"
        for value in split:
            if(i == 0):
                tableString += "<th>%s</th>" % value
            else:
                tableString += "<td>%s</td>" % value
        tableString += "</tr>"
    tableString += "</table>"
    return tableString

# metrics implementation
def precision_at_k(r, k):
    assert k >= 1
    r = np.asarray(r)[:k] != 0
    return np.mean(r)

# average precision 1 from the script
def average_precision(r):
    r = np.asarray(r) != 0
    out = [precision_at_k(r, k + 1) for k in range(r.size) if r[k]]
    if not out: return 0
    return np.mean(out)

# mean average precision on result set
def mean_average_precision(rs):
    return np.mean([average_precision(r) for r in rs])

# read and parse log file
def generate_resultData(logLines, delim):
    # init
    resultData = {}
    currentSearchTerm = {}
    resultSet = []
    timestamps = {}
    for i, line in enumerate(logLines):
        # ignore header
        if(i != 0):
            split = re.split(delim, line)
            user = split[0]
            ts = split[1]
            action = split[2]
            payload = split[3].strip()

            # Set current search term for user
            # example line: 172.18.18.1	2019-02-20 16:16:13.510290	SEARCH_TERM	Siemens
            if(action == "SEARCH_TERM"):
                currentSearchTerm[user] = payload

            # save result relevance
            # example line: 172.18.18.1	2019-02-20 16:16:14.296920	RELEVANCE_ARRAY	0,0,0,0,0,0,0,0,0,0
            if(action == "RELEVANCE_INIT"):
                k = int(payload)
                relevances = []
                for x in range(0,k):
                    relevances.append(0)
                if user in resultData:
                    resultData[user][currentSearchTerm[user]] = relevances
                else:
                    resultData[user] = {currentSearchTerm[user] : relevances}

            # Pagination -> vergrößere Relevanz-Array
            # example line: 172.18.18.1	2019-02-20 16:16:51.724370	PAGINATION	10
            if(action == "PAGINATION"):
                k = int(payload)
                for x in range(0,k):
                    resultData[user][currentSearchTerm[user]].append(0)

            # example line: 172.18.18.1	2019-02-20 16:16:51.724370	SNIPPET_CLICK	2
            if(action == "SNIPPET_CLICK"):
                clickedResult = int(payload)
                #resultData[user][currentSearchTerm[user]][clickedResult] = 1
                if user in timestamps:
                    timestamps[user][currentSearchTerm[user]] = datetime.datetime.strptime(ts, '%Y-%m-%d %H:%M:%S.%f')
                else:
                    timestamps[user] = {currentSearchTerm[user] : datetime.datetime.strptime(ts, '%Y-%m-%d %H:%M:%S.%f')}

            # Modal is closed -> if it was open longer than 10 seconds, assume it was relevant
            # example line: 172.18.18.1	2019-02-20 16:16:51.724370	MODAL_CLOSED	2
            if(action == "MODAL_CLOSED"):
                clickedResult = int(payload)
                modalOpened = timestamps[user][currentSearchTerm[user]]
                modalClosed = datetime.datetime.strptime(ts, '%Y-%m-%d %H:%M:%S.%f')
                dwell_time = modalClosed - modalOpened
                if(dwell_time.total_seconds() > 10):
                    resultData[user][currentSearchTerm[user]][clickedResult] = 1

            # direct user feedback
            # example line: 172.18.18.1	2019-02-20 16:16:52.959975	USER_RATED	2,1
            if(action == "USER_RATED"):
                result = int(payload.split(',')[0])
                relevance = int(payload.split(',')[1])
                resultData[user][currentSearchTerm[user]][result] = relevance
    return resultData

# Generates Resultset for Mean Average Precision
def generate_resultSet(resultData):
    resultSet = []
    for user in resultData:
        for searchTerm in resultData[user]:
            resultSet.append(resultData[user][searchTerm])
    return resultSet

###
# Query and Caching
###
r = redis.StrictRedis(host=redisIp, port=6379)

def push(key, data):
    try:
        r.execute_command('JSON.SET', key, '.', json.dumps(data))
        return True
    except:
        return False

def pop(key):
    try:
        reply = json.loads(r.execute_command('JSON.GET', key))
    except:
        return []
    return reply

solr = pysolr.Solr('http://%s:8983/solr/companies/' % solrIp)

# Main Query
def query(query, offset, maximum):
    o_query = query.replace(' ','*')
    results = solr.search(
            '(name:(*%s*) OR name:"%s") OR text:(*%s*) OR (industry_label:(*%s*) OR keyPerson_label:(*%s*) OR product_label:(*%s*) OR locationCity_label:(*%s*) OR locationCountry_label:(*%s*))' % (query,query,query,o_query,o_query,o_query,o_query,o_query), 
            #'(name:(*%s*) OR name:"%s") OR text:(*%s*) OR (industry:(*%s*) OR keyPerson:(*%s*) OR product:(*%s*) OR locationCity:(*%s*) OR locationCountry:(*%s*))' % (query,query,query,query,query,query,query,query), 
            fq = ['name:[* TO *]'], 
            fl = "id,label,name,comment,text", 
            rows=maximum, 
            start=offset
        )
    obj = results.raw_response
    push(query, obj)
    return obj

# query for auto suggestion
def querySuggest(query):
    results = solr.search(
            'name:(*%s*) OR name:"%s"' % (query,query), 
            fq = ['name:[* TO *]'], 
            fl = "name", 
            rows=10, 
            start=0
        )
    obj = results.raw_response
    push(query, obj )
    return obj


###
# Logging
###
if not os.path.exists("logs"): os.makedirs("logs")
open("./logs/log.tsv","w+").write("ip\ttimestamp\taction\tpayload\n")
def log(ip, action, payload):
    ts = str(datetime.datetime.now())
    logFile = open("./logs/log.tsv","a")
    logFile.writelines("%s\t%s\t%s\t%s\n" % (ip,ts, action, payload))
    return


###
#   API CORS
###
app = Flask(__name__)
app.config['CORS_HEADERS'] = "Content-Type"
app.config['CORS_RESOURCES'] = {r"/*": {"origins": "*"}}
CORS(app)


###
#   API routes
###
# Daten Anfrage
@app.route("/get/<business_label>")
def getBusiness(business_label):
    log(request.remote_addr, "GET_DATA", business_label)
    return jsonify(solr.search("label:%s" % business_label).raw_response)

# Suchanfrage
@app.route("/search/<offset>/<maximum>/<term>")
def searchBusinessPartly(offset, maximum, term):
    log(request.remote_addr, "SEARCH_TERM", term)
    if(offset == "0"):
        log(request.remote_addr, "RELEVANCE_INIT", maximum)
    return jsonify(query(term, offset, maximum))

# Autocomplete
@app.route("/autocomplete/<term>")
def autocompleteBusiness(term):
    obj = querySuggest(term)
    #log(request.remote_addr, "AUTOCOMPLETE", term)
    return jsonify(obj)

# Click auf ein Ergebnis
@app.route("/snippetClicked/<index>")
def snippetClicked(index):
    log(request.remote_addr, "SNIPPET_CLICK", index)
    return index

# Ergebnis geschlossen
@app.route("/modalClosed/<index>")
def modalClosed(index):
    log(request.remote_addr, "MODAL_CLOSED", index)
    return index

# Pagination
@app.route("/pagination/<maximum>")
def pagination(maximum):
    log(request.remote_addr, "PAGINATION", maximum)
    return maximum

# Active Relevance Feedback
@app.route("/userRated/<index>/<rating>")
def userRating(index, rating):
    log(request.remote_addr, "USER_RATED", index + "," + rating)
    return index + "," + rating

# Display Log
@app.route("/log/")
def showLog():
    logFile = open("./logs/log.tsv","r")
    logString = "<!DOCTYPE html><html><body>"
    logString += toHTMLTable(logFile.readlines(), "\t+")
    logString += "</body></html>"
    return str(logString)

# Display Evaluation
@app.route("/eval/")
def evaluation():
    logLines = open("./logs/log.tsv","r").readlines()
    resultData = generate_resultData(logLines, "\t+")
    tableData = ["user \t term \t result \t average precision"]
    for user in resultData:
        for term in resultData[user]:
            result = resultData[user][term]
            avg = average_precision(result)
            tableData.append("%s\t%s\t%s\t%s" % (user, term, result, avg))
    meanAvgP = mean_average_precision(generate_resultSet(resultData))
    html = "<!DOCTYPE html><html><body>"
    html += toHTMLTable(tableData, "\t+")
    html += "<p>Mean Average Precision: %s</p>" % (meanAvgP)
    html += "</body></html>"
    return str(html)

###
# Main
###
if __name__ == "__main__":
    app.run(host='0.0.0.0', port=5000)
