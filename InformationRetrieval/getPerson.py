import sys
import time
import json
from query import *
from SPARQLWrapper import SPARQLWrapper, JSON


ReqSize = 5000
sparql = SPARQLWrapper("http://dbpedia.org/sparql")
sparql.setReturnFormat(JSON)

sparql2 = SPARQLWrapper("http://akswnc7.informatik.uni-leipzig.de:8890/sparql")
sparql2.setReturnFormat(JSON)

comanies_count = 0
with_full_text = 0


def resolve_iri_suffix(uri):
    uri = uri.rsplit('/', 1)[-1].rsplit('#', 1)[-1]
    uri = uri.replace("_", " ")
    return uri


def get_company_list():
    company_list = []
    i = 0
    while True:
        size = len(company_list)
        sparql.setQuery(get_company_related_person_list_query(i*ReqSize, ReqSize))
        company_list += sparql.query().convert()["results"]["bindings"]
        i += 1
        if(len(company_list) == size):
            break

    return company_list


def get_company_data(companyIRI):
    sparql.setQuery(get_resource_data_query(companyIRI))
    response = sparql.query().convert()["results"]["bindings"]

    tmp = {}
    for row in response:
        property = str(resolve_iri_suffix(row["p"]["value"]))
        value = str(row["o"]["value"])
        tmp.setdefault(property, []).append(value)

    return tmp


def get_wiki_full_text(companyIRI):
    nifIRI = companyIRI + '?dbpv=2016-10&nif=context'
    sparql2.setQuery(get_wiki_full_text_query(nifIRI))
    response = sparql2.query().convert()["results"]["bindings"]

    tmp = {}
    for row in response:
        value = str(row["text"]["value"])
        tmp.setdefault("text", []).append(value)

    global with_full_text
    with_full_text += 1
    return tmp


if __name__ == "__main__":

    with open('./company_persons.json', 'a', encoding='utf-8') as outfile:
        c = 0
        for item in get_company_list():
            companyIRI = item['company']['value']
            company = {}

            company.update({'id': companyIRI})
            
            try:
                company.update(get_company_data(companyIRI))
            except Exception as err1:
                time.sleep(5)
                try:
                    company.update(get_company_data(companyIRI))
                except Exception as err2:
                    print(err2)

            try:
                company.update(get_wiki_full_text(companyIRI))
            except Exception as err1:
                time.sleep(5)
                try:
                    company.update(get_wiki_full_text(companyIRI))
                except Exception as err2:
                    print(err2)

            outfile.write(json.dumps(company) + '\n')
            c += 1
            sys.stdout.flush()
            sys.stdout.write("acquired {0} \r".format(c))

    print("with full text {0}".format(with_full_text))

