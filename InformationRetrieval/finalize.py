import sys
import json
from bz2 import BZ2File as bzopen

weight_dict = {}
uri_to_label = {}

uri_to_label_properties = ['keyPerson', 'industry', 'locationCity', 'locationCountry', 'product']

if __name__ == '__main__':

    weight_file = open(sys.argv[1])
    json_file = bzopen(sys.argv[2])

    for line in weight_file:
        id = line.split(' ')[0]
        we = line.split(' ')[1]
        weight_dict[id] = weight_dict.get(id,0) + int(we)

    c = 0
    with open(sys.argv[2]+'.final', 'w', encoding='utf-8') as outfile:
        for line in json_file:
            curobj = json.loads(line)

            id = str(curobj['id'])
            if (curobj.get('text','') != '' )  and (curobj.get('label','') != '' ):

                try:
                    id = id.split('/')[4]
                except Exception as err:
                    print(err)

                curobj['spotlight_weight'] = weight_dict.get(id,0)

                for p in uri_to_label_properties:
                    for uri in curobj.get(p,[]):
                        alt_label = uri.split('/')[-1:][0].replace('_',' ')
                        curobj.setdefault(p+'_label', []).append(alt_label)

                outfile.write(json.dumps(curobj)+'\n')

                c += 1
                sys.stdout.flush()
                sys.stdout.write("pos {0} \r".format(c))

