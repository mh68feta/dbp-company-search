import json
import requests
import sys
import time
import re
from bz2 import BZ2File as bzopen

file = bzopen(sys.argv[1])

url = 'http://localhost:2222/rest/annotate'
# types = 'Company,Person,Organisation'

spotlight_weight = {}

def spotlight(id,text):
    payload = {'text': text,
               'confidence':'0.5'}
    headers = {'Accept': 'text/html'}
    r = ""
    try:
        response = requests.post(url, data=payload, headers=headers)
        if( response.status_code == 200 ):
            r = response.text.split("\n")[8]
        else:
            print("ERROR {0}: {1}".format(id,response.status_code))
    except Exception as ex:
        print(ex)
    return r


if __name__ == '__main__':

    start_time = time.time()

    with open(sys.argv[1]+'.spotlight', 'w', encoding='utf-8') as outfile:
        c = 1
        for line in file:
            curobj = json.loads(line)
            if 'text' in curobj:
                spotlight_text = spotlight(curobj['id'],curobj['text'])
                curobj.update({'spotlight_text': spotlight_text})

                spotlight_links = list(set(re.findall('(?<=<a href=")http://dbpedia.org/resource/\w*', spotlight_text)))
                curobj.update({'spotlight_links': spotlight_links})

                for link in spotlight_links:
                    key = link.split('/')[4]
                    spotlight_weight[key] = spotlight_weight.get(key,0) + 1

                outfile.write(json.dumps(curobj) + '\n')

                c += 1
                # if( c % 10 == 0):
                sys.stdout.flush()
                sys.stdout.write('pos: {0} time: {1} sec/page \r'.format(c,round((time.time()-start_time)/c,3)))

                if( c == 10 ):
                    break

    with open(sys.argv[1]+'.weight', 'w', encoding='utf-8') as outfile:
        for k in spotlight_weight:
            outfile.write("{0} {1}\n".format(k,spotlight_weight[k]))
