# Data Acquisition and Preprocessing

This folder contains the python source code which was used to acquire the domain specific documents based on dbpedia dumps.

Used for relvant Company data acquisition.
```
python3 getData.py
python3 getPerson.py
python3 getPlace.py
python3 getIndustry.py
python3 getProduct.py
``` 
These Scripts need a configured triple store containing the described data in the report.

Used to [Spotlight](https://github.com/dbpedia-spotlight/spotlight-docker) Data.
```
python3 spotData.py <acquied json file>
```

Cleanup Skript, to add spotlight weights and resolve labels for defined uri fields.
```
python3 finalize.py <weight tsv> <spotlighted json>

```