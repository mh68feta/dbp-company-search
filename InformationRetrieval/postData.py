#!/usr/bin/python3
import os
import sys
import json
import pysolr
import json_lines
from bz2 import BZ2File as bzopen

file = bzopen(sys.argv[1])

solrIp = 'localhost'
solr = pysolr.Solr('http://%s:8983/solr/companies/' % solrIp)

i=1
failed=1

for line in file:
	curobj = json.loads(line)

	try:
		solr.add([curobj])
	except Exception as err:
		print("ERROR {0} : {1} ".format(curobj['id'],err))
		failed += 1
		continue

	sys.stdout.flush()
	sys.stdout.write("pos {0} \r".format(i))

	i += 1

	if(i % 1000 == 0):
		solr.commit()

solr.commit()

print('posted', i-failed)
print('failed', failed)
