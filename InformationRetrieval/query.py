# This file defines the used  sparql queries
# to request the domain specific company data
# from DBpedias RDF dumps.

prefixes = """
PREFIX dbo: <http://dbpedia.org/ontology/>
PREFIX nif: <http://persistence.uni-leipzig.org/nlp2rdf/ontologies/nif-core#>
"""

def get_company_list_query(offset, limit):
    return prefixes + """
    SELECT * {
        ?company a dbo:Company .
    }
    OFFSET %s
    LIMIT %s
    """ % (offset, limit)


def get_company_related_person_list_query(offset, limit):
    return prefixes + """
    SELECT distinct ?company {
    ?s a dbo:Company .
    ?s ?p ?company .
    ?company a dbo:Person .
    FILTER ( ! strStarts(str(?p), "http://dbpedia.org/property") )
    }
    ORDER BY ?company
    OFFSET %s
    LIMIT %s
    """ % (offset, limit)


def get_company_related_place_list_query(offset, limit):
    return prefixes + """
    SELECT distinct ?company {
    ?s a dbo:Company .
    ?s ?p ?company .
    ?company a dbo:Place .
    FILTER ( ! strStarts(str(?p), "http://dbpedia.org/property") )
    } 
    ORDER BY ?company
    OFFSET %s
    LIMIT %s
    """ % (offset, limit)


def get_company_related_product_list_query(offset, limit):
    return prefixes + """
    SELECT distinct ?company {
    ?s a dbo:Company .
    ?s dbo:product ?company .
    }
    OFFSET %s
    LIMIT %s
    """ % (offset, limit)


def get_company_related_industry_list_query(offset, limit):
    return prefixes + """
    SELECT distinct ?company {
    ?s a dbo:Company .
    ?s dbo:industry ?company .
    }
    OFFSET %s
    LIMIT %s
    """ % (offset, limit)


def get_resource_data_query(iri):
    return prefixes + """
    SELECT * {
        <%s> ?p ?o .
        FILTER ( ! strStarts(str(?p), "http://dbpedia.org/property") )
        BIND( IF( isIRI(?o), "", lang(?o) ) as ?language )
        FILTER (  ?language = "" || ?language = "en" )
    }
    """ % iri


def get_wiki_full_text_query(iri):
    return prefixes + """
    SELECT  ?text {
        <%s> nif:isString ?text .
        }
    """ % iri
