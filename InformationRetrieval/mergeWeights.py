import sys

weight_ditc = {}

if __name__ == '__main__':
    file_names = sys.argv

    for file_name in file_names[1:]:
        print(file_name)

        file = open(file_name)
        for line in file:
            id = str(line).split(' ')[0]
            we = str(line).split(' ')[1]
            weight_ditc[id] = weight_ditc.get(id,0) + int(we)

    with open('./weight_tsv', 'a', encoding='utf-8') as outfile:

        for k in weight_ditc:
            outfile.write("{0} {1}\n".format(k,weight_ditc[k]))
