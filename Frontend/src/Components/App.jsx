import React, { Component } from 'react';
import {Autocomplete, Pagination, Button, Row} from 'react-materialize';
import axios from 'axios';
import Snipped from './Snipped';

import Config from '../Config.json'


class App extends Component { 

  constructor(props){
    super(props);
    this.state = {
        ...Config,
        currentpage: 1,
        searchtime: .0,
        searchterm: '',
        suggestion: {},
        searchdata: [],
        numberRes: 0,
        results: []
      };
    this.updateItems = this.updateItems.bind(this);
  }

  autoSuggest(str) {
    axios.get(
      this.state.backendAutocompletePath+str
    ).then(res => {
      this.suggestion = {}
      res.data.response.docs.forEach((doc) => {
        this.suggestion[doc.name] = null
      })
      this.setState({suggestion:this.suggestion})
    })
  }

  updateItems(str, offset, max){
    this.setState({searchterm:str})
    this.setState({suggestion:{}})
    var t0 = performance.now()
    axios.get(
      this.state.backendSearchPath+
      max*(offset-1)+"/"+max+"/"+str
    )
    .then(res => {
      this.setState({ 
        results: []
      })
      this.setState({ 
        results: res.data.response.docs,
        numberRes: res.data.response.numFound,
        searchtime: ((performance.now()-t0)/1000).toFixed(4)
      })
      return res;
    })
  }

  render() {
    return (
      <div style = {{margin: 'auto auto auto auto', maxWidth: "1080px"}}>
        { // Searchbar
          <form onSubmit={(e) => {
            e.preventDefault()
            this.setState({results : [], currentpage:1})
            if(this.state.searchterm !== ""){
              this.updateItems(this.state.searchterm, 1, this.state.resultsperpage)
            }
            }}>
            <div className="card-panel lighten-5 z-depth-1">
              <Row>
                <Autocomplete
                  minLength={1}
                  style={{width: "90%"}}
                  data = {this.state.suggestion}
                  value = {this.state.searchterm}
                  onChange = {(e) => {
                    this.setState({currentpage:1, searchterm : e.target.value})
                    if(this.state.searchterm !== ""){
                      this.autoSuggest(this.state.searchterm)
                    }
                    }
                  }
                  onAutocomplete = {(e) => {
                    if(this.state.searchterm !== ""){
                      this.setState({results : []})
                      this.updateItems(e, 1, this.state.resultsperpage)
                      this.value = e
                    }
                  }}
                />
                <Button floating small className='gray' waves='light' icon='search' type="submit"/>
              </Row>
              <p>{this.state.numberRes} Results ({this.state.searchtime} sec)</p>
            </div>
          </form>
        }
        { // Results
          this.state.results.map(
                (result,i) => 
                <Snipped key={i} data={result} query={this.state.searchterm} index={i * this.state.currentpage}/>
            )
        }
        { // Pagination
          this.state.results.length > 0 ?
            <Pagination className="center bottom"
              items={Math.floor(this.state.numberRes / this.state.resultsperpage + 1)}
              activePage={this.state.currentpage}
              maxButtons={10}
              onSelect={item => {
                  if(item > this.state.currentpage){
                    axios.get(this.state.backendPaginationPath + this.state.resultsperpage)
                  }
                  this.setState({currentpage: item})
                  this.updateItems(this.state.searchterm, item, this.state.resultsperpage)
                }}
            />
            :
            null
        }
      </div>    
    );
  }
}

export default App;
