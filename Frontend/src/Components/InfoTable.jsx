import React, { Component } from 'react';
import {Table} from 'react-materialize';
import axios from 'axios';
import Map from './Map'
import Config from '../Config.json'

class InfoTable extends Component { 

  constructor(props){
    super(props);
    this.state = {
      ...Config,
      id: this.props.id,
    }
  }

  componentDidMount() {
    axios.get(this.state.backendGetPath+this.state.id[0])
    .then(res => this.setState({...res.data.response.docs[0]}))
  }

  render() {
    return (
        <div>
            { // Map if location data is present
                this.state.lat != null && this.state.long != null ?
                <Map lat={parseFloat(this.state.lat)} lng={parseFloat(this.state.long)}/>
                :
                null
            }

            <Table
                bordered={true}
                hoverable={true}
            >
                <tbody>
                    { // slogan
                        this.state.slogan != null ?
                        <tr>
                            <td>{"Slogan"}</td> 
                            <td>{this.state.slogan}</td>
                        </tr>
                        :
                        null
                    }
                    { // parent
                        this.state.parent != null ?
                        <tr>
                            <td>{"Parent"}</td> 
                            <td>{this.state.parent}</td>
                        </tr>
                        :
                        null
                    }
                    { // assets
                        this.state.assets != null ?
                        <tr>
                            <td>{"Assets"}</td> 
                            <td>{this.state.assets}$</td>
                        </tr>
                        :
                        null
                    }
                    { // equity
                        this.state.equity != null ?
                        <tr>
                            <td>{"Equity"}</td> 
                            <td>{this.state.equity}$</td>
                        </tr>
                        :
                        null
                    }
                    { // netIncome
                        this.state.netIncome != null ?
                        <tr>
                            <td>{"Net income"}</td> 
                            <td>{this.state.netIncome}$</td>
                        </tr>
                        :
                        null
                    }
                    { // operatingIncome
                        this.state.operatingIncome != null ?
                        <tr>
                            <td>{"OperatingIncome"}</td> 
                            <td>{this.state.operatingIncome}$</td>
                        </tr>
                        :
                        null
                    }
                    { // foundingYear
                        this.state.foundingYear != null ?
                        <tr>
                            <td>{"Founding year"}</td> 
                            <td>{this.state.foundingYear}</td>
                        </tr>
                        :
                        null
                    }
                    { // openingDate
                        this.state.openingDate != null ?
                        <tr>
                            <td>{"Opening date"}</td> 
                            <td>{this.state.openingDate}</td>
                        </tr>
                        :
                        null
                    }
                    { // founded
                        this.state.founded != null ?
                        <tr>
                            <td>{"Founded"}</td> 
                            <td>{this.state.founded}</td>
                        </tr>
                        :
                        null
                    }
                    { // ceased
                        this.state.ceased != null ?
                        <tr>
                            <td>{"Ceased"}</td> 
                            <td>{this.state.products}</td>
                        </tr>
                        :
                        null
                    }
                    { // extinctionYear
                        this.state.extinctionYear != null ?
                        <tr>
                            <td>{"Extinction year"}</td> 
                            <td>{this.state.extinctionYear}</td>
                        </tr>
                        :
                        null
                    }
                    { // numberOfEmployees
                        this.state.numberOfEmployees != null ?
                        <tr>
                            <td>{"Employees"}</td> 
                            <td>{this.state.numberOfEmployees}</td>
                        </tr>
                        :
                        null
                    }
                    { // revenue
                        this.state.revenue != null ?
                        <tr>
                            <td>{"Revenue"}</td> 
                            <td>{this.state.revenue}$</td>
                        </tr>
                        :
                        null
                    }
                    { // areaServed
                        this.state.areaServed != null ?
                        <tr>
                            <td>{"Served area"}</td> 
                            <td>{this.state.areaServed}</td>
                        </tr>
                        :
                        null
                    }
                    { // keyPeople
                        this.state.keyPeople != null ?
                        <tr>
                            <td>{"Key people"}</td> 
                            <td>{this.state.keyPeople}</td>
                        </tr>
                        :
                        null
                    }
                    { // ceo
                        this.state.ceo != null ?
                        <tr>
                            <td>{"CEO"}</td> 
                            <td>{this.state.ceo}</td>
                        </tr>
                        :
                        null
                    }
                    { // owner
                        this.state.owner != null ?
                        <tr>
                            <td>{"Owner"}</td> 
                            <td>{this.state.owner}</td>
                        </tr>
                        :
                        null
                    }
                    { // services
                        this.state.services != null ?
                        <tr>
                            <td>{"Services"}</td> 
                            <td>{this.state.services}</td>
                        </tr>
                        :
                        null
                    }
                    { // industry
                        this.state.industry != null ?
                        <tr>
                            <td>{"Industry"}</td> 
                            <td>{this.state.industry}</td>
                        </tr>
                        :
                        null
                    }
                    { // products
                        this.state.products != null ?
                        <tr>
                            <td>{"Products"}</td> 
                            <td>{this.state.products}</td>
                        </tr>
                        :
                        null
                    }
                    { // headquarters
                        this.state.headquarters != null ?
                        <tr>
                            <td>{"Headquarters"}</td> 
                            <td>{this.state.headquarters}</td>
                        </tr>
                        :
                        null
                    }
                    { // marketCap
                        this.state.marketCap != null ?
                        <tr>
                            <td>{"Market capacity"}</td> 
                            <td>{this.state.products}</td>
                        </tr>
                        :
                        null
                    }
                    { // aoc
                        this.state.aoc != null ?
                        <tr>
                            <td>{"aoc"}</td> 
                            <td>{this.state.aoc}</td>
                        </tr>
                        :
                        null
                    }
                    { // origin
                        this.state.origin != null ?
                        <tr>
                            <td>{"Origin"}</td> 
                            <td>{this.state.origin}</td>
                        </tr>
                        :
                        null
                    }
                    { // endYear
                        this.state.endYear != null ?
                        <tr>
                            <td>{"End year"}</td> 
                            <td>{this.state.endYear}</td>
                        </tr>
                        :
                        null
                    }
                    { // width
                        this.state.width != null ?
                        <tr>
                            <td>{"Width"}</td> 
                            <td>{this.state.width}</td>
                        </tr>
                        :
                        null
                    }
                    { // runtime
                        this.state.runtime != null ?
                        <tr>
                            <td>{"Runtime"}</td> 
                            <td>{this.state.runtime}</td>
                        </tr>
                        :
                        null
                    }
                    { // introduced
                        this.state.introduced != null ?
                        <tr>
                            <td>{"Introduced"}</td> 
                            <td>{this.state.introduced}</td>
                        </tr>
                        :
                        null
                    }
                    { // launchDate
                        this.state.launchDate != null ?
                        <tr>
                            <td>{"Launch date"}</td> 
                            <td>{this.state.launchDate}</td>
                        </tr>
                        :
                        null
                    }
                    { // established
                        this.state.established != null ?
                        <tr>
                            <td>{"established"}</td> 
                            <td>{this.state.established}</td>
                        </tr>
                        :
                        null
                    }
                    { // ceased
                        this.state.ceased != null ?
                        <tr>
                            <td>{"Ceased"}</td> 
                            <td>{this.state.ceased}</td>
                        </tr>
                        :
                        null
                    }
                    { // numEmployeesYear
                        this.state.numEmployeesYear != null ?
                        <tr>
                            <td>{"#Employees/Year"}</td> 
                            <td>{this.state.numEmployeesYear}</td>
                        </tr>
                        :
                        null
                    }
                    { // fleetSize
                        this.state.fleetSize != null ?
                        <tr>
                            <td>{"Fleet size"}</td> 
                            <td>{this.state.fleetSize}</td>
                        </tr>
                        :
                        null
                    }
                    { // latestReleaseDate
                        this.state.latestReleaseDate != null ?
                        <tr>
                            <td>{"Latest release date"}</td> 
                            <td>{this.state.latestReleaseDate}</td>
                        </tr>
                        :
                        null
                    }
                    { // operatingIncome
                        this.state.operatingIncome != null ?
                        <tr>
                            <td>{"Operating income"}</td> 
                            <td>{this.state.operatingIncome}</td>
                        </tr>
                        :
                        null
                    }
                    { // established
                        this.state.established != null ?
                        <tr>
                            <td>{"Established"}</td> 
                            <td>{this.state.established}</td>
                        </tr>
                        :
                        null
                    }
                    { // weight
                        this.state.weight != null ?
                        <tr>
                            <td>{"Weight"}</td> 
                            <td>{this.state.weight}</td>
                        </tr>
                        :
                        null
                    }
                    { // assets
                        this.state.assets != null ?
                        <tr>
                            <td>{"Assets"}</td> 
                            <td>{this.state.assets}$</td>
                        </tr>
                        :
                        null
                    }
                    { // retired
                        this.state.retired != null ?
                        <tr>
                            <td>{"Retired"}</td> 
                            <td>{this.state.retired}</td>
                        </tr>
                        :
                        null
                    }
                    { // screenshot
                        this.state.screenshot != null ?
                        <tr>
                            <td>{"Screenshot"}</td> 
                            <td>{this.state.screenshot}</td>
                        </tr>
                        :
                        null
                    }
                    { // foundation
                        this.state.foundation != null ?
                        <tr>
                            <td>{"Foundation"}</td> 
                            <td>{this.state.foundation}</td>
                        </tr>
                        :
                        null
                    }
                    { // equity
                        this.state.equity != null ?
                        <tr>
                            <td>{"Equity"}</td> 
                            <td>{this.state.equity}$</td>
                        </tr>
                        :
                        null
                    }
                    { // birthDate
                        this.state.birthDate != null ?
                        <tr>
                            <td>{"Birth date"}</td> 
                            <td>{this.state.birthDate}</td>
                        </tr>
                        :
                        null
                    }
                    { // stations
                        this.state.stations != null ?
                        <tr>
                            <td>{"Stations"}</td> 
                            <td>{this.state.stations}</td>
                        </tr>
                        :
                        null
                    }
                    { // depots
                        this.state.depots != null ?
                        <tr>
                            <td>{"Depots"}</td> 
                            <td>{this.state.depots}</td>
                        </tr>
                        :
                        null
                    }
                    { // hubs
                        this.state.hubs != null ?
                        <tr>
                            <td>{"Hubs"}</td> 
                            <td>{this.state.hubs}</td>
                        </tr>
                        :
                        null
                    }


                    { // equity
                        this.state.equity != null ?
                        <tr>
                            <td>{"Equity"}</td> 
                            <td>{this.state.equity}</td>
                        </tr>
                        :
                        null
                    }
                    { // chemspiderid
                        this.state.chemspiderid != null ?
                        <tr>
                            <td>{"ChemspiderID"}</td> 
                            <td>{this.state.chemspiderid}</td>
                        </tr>
                        :
                        null
                    }
                    { // district
                        this.state.district != null ?
                        <tr>
                            <td>{"District"}</td> 
                            <td>{this.state.district}</td>
                        </tr>
                        :
                        null
                    }
                    { // commenced
                        this.state.commenced != null ?
                        <tr>
                            <td>{"Commenced"}</td> 
                            <td>{this.state.commenced}</td>
                        </tr>
                        :
                        null
                    }
                    { // birthDate
                        this.state.birthDate != null ?
                        <tr>
                            <td>{"Birth date"}</td> 
                            <td>{this.state.birthDate}</td>
                        </tr>
                        :
                        null
                    }
                    { // teaImage
                        this.state.teaImage != null ?
                        <tr>
                            <td>{"Tea image"}</td> 
                            <td>{this.state.teaImage}</td>
                        </tr>
                        :
                        null
                    }
                    { // startYear
                        this.state.startYear != null ?
                        <tr>
                            <td>{"Start year"}</td> 
                            <td>{this.state.startYear}</td>
                        </tr>
                        :
                        null
                    }
                    { // close
                        this.state.close != null ?
                        <tr>
                            <td>{"Close"}</td> 
                            <td>{this.state.close}</td>
                        </tr>
                        :
                        null
                    }
                    { // casesPerYear
                        this.state.casesPerYear != null ?
                        <tr>
                            <td>{"#Cases per year"}</td> 
                            <td>{this.state.casesPerYear}</td>
                        </tr>
                        :
                        null
                    }
                    { // dimensions
                        this.state.dimensions != null ?
                        <tr>
                            <td>{"Dimensions"}</td> 
                            <td>{this.state.dimensions}</td>
                        </tr>
                        :
                        null
                    }
                </tbody>
            </Table>
        </div>
    );
  }
}

export default InfoTable