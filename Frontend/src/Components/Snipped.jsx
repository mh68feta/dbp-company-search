import React, { Component } from 'react';
import {Modal, Card, Button} from 'react-materialize';
import TextHighlight from 'react-text-highlight';
import Config from '../Config.json'
import InfoTable from './InfoTable'
import axios from 'axios';

class Snipped extends Component { 
  
  constructor(props){
    super(props);
    this.state = {
      ...Config,
      ...this.props.data,
      query: this.props.query,
      rated: false,
      open: false
    }
  }

  snippetClick = () => {
    if(!this.state.rated) {
      axios.get(this.state.backendSnippetClickedPath+this.props.index)
      console.log("snippet clicked ", this.state.name)
    }
  }

  modalCloseClick = () => {
    if(!this.state.rated) {
      axios.get(this.state.backendModalCloseClick+this.props.index)
      console.log("modal closed ", this.state.name)
    }
  }

  relevance = (rating) => {
    this.setState({rated : true})
    axios.get(this.state.backendUserRatedPath+this.props.index+"/" +rating)
    if(rating === 1) {
      window.Materialize.toast('marked as relevant!', 450)
      console.log("marked as true positive", this.state.name)
    } else {
      window.Materialize.toast('marked as irrelevant!', 450)
      console.log("marked as false positive", this.state.name)
    }
  }

  render() {
    return (
      <Modal
        id="foo"
        header={this.state.name.toString()}
        trigger=
        { // Preview (Snipped)
          <Card>
            <div onClick={() => this.snippetClick() /*find better way to do this*/}>
              <a>{this.state.name}</a>
              <br/>
              <TextHighlight
                highlight={this.state.query}
                text={this.state.comment.toString()}
              />
            </div>
          </Card>
        }
        open={this.state.open}
        actions={[
            <Button floating className='red' waves='light' icon='arrow_downward' onClick={() => this.relevance(0)}/>,
            <Button floating className='green' waves='light' icon='arrow_upward' onClick={() => this.relevance(1)}/>,
            <Button 
              style={{marginLeft: "10px", marginRight: "80%"}} 
              onClick={() => {this.modalCloseClick(), this.setState({open : false})}}
            >
              close
            </Button>
          ]}
      >
        {<a href={this.state.url}>{this.state.url}</a>}
        { // Text
          this.state.text != null ?
            <TextHighlight
              highlight={this.state.query}
              text={this.state.text.toString()}
            />
            :
            <TextHighlight
              highlight={this.state.query}
              text={this.state.comment.toString()}
            />
        }
        { // Additional unsorted Information
          <InfoTable id={this.state.label} />
        }
      </Modal>
    );
  }
}

export default Snipped